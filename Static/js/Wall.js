function Wall(){
    var container = new THREE.Object3D()
    var WallGeometry = new THREE.BoxGeometry(200, 100, 10);
    var WallMaterial = new THREE.MeshPhongMaterial({
        map: new THREE.TextureLoader().load('../models/wall.jpg'),
        side: THREE.DoubleSide,
        wireframe: false,
        transparent: true,
        shininess: 10
    })
    var Wall = new THREE.Mesh(WallGeometry, WallMaterial)
    Wall.position.y = 50
    container.add(Wall)
    this.getWallCont = function () {
        return container
    }
}