function Main() {
    var that = this;
    var scene = new THREE.Scene()
    var time
    var camera = new THREE.PerspectiveCamera(
        45, // kąt patrzenia kamery (FOV - field of view)
        $(window).width() / $(window).height(), // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
        0.1, // minimalna renderowana odległość
        10000 // maxymalna renderowana odległość
    )
    var renderer = new THREE.WebGLRenderer({
        antialias: true
    })
    var looking = false;
    renderer.setClearColor(0x73ADD6)
    renderer.setSize($(window).width(), $(window).height())

    $("#root").append(renderer.domElement)
    var userName;
    var ip;

    var board = new Board()
    scene.add(board.getBoardCont())

    var player = new Player();

    var lights = new Lights();
    scene.add(lights.getLights());

    camera.position.set(
        0,
        player.height,
        0
    )
    camera.lookAt(0, player.height, 0);
    camera.updateProjectionMatrix();
    camera.rotation.y = -Math.PI / 2;

    // var orbitControl = new THREE.OrbitControls(camera, renderer.domElement);
    // orbitControl.addEventListener('change', function () {
    //     renderer.render(scene, camera)
    // });
    var endTime = 0
    var stats = new Stats();
    stats.showPanel(0);
    document.body.appendChild(stats.dom);

    var raycaster = new THREE.Raycaster(); // obiekt symulujący "rzucanie" promieni
    var mouseVector = new THREE.Vector2() // ten wektor czyli pozycja w przestrzeni 2D na ekranie(x,y) wykorzystany będzie do określenie pozycji myszy na ekranie a potem przeliczenia na pozycje 3D

    $(document).click(function (event) {
        mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
        mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
        raycaster.setFromCamera(mouseVector, camera);
        var intersects = raycaster.intersectObjects(scene.children, true);
        if (intersects.length > 0) {
            if (intersects[0].object.userData.a === "lever") {
                var leverData = intersects[0].object.userData;
                console.log(arr[leverData.id]);

                $('.edit').css("display", "flex");

                if (arr[leverData.id].north == "no") {
                    $('.edit .north').css("display", "none");
                }
                if (arr[leverData.id].west == "no") {
                    $('.edit .west').css("display", "none");
                }
                if (arr[leverData.id].south == "no") {
                    $('.edit .south').css("display", "none");
                }
                if (arr[leverData.id].east == "no") {
                    $('.edit .east').css("display", "none");
                }

                $('.edit .west').on('click', function () {
                    $(this).css("background", "rgb(110, 110, 110)")
                    arr[leverData.id].west = "no";
                    scene.remove(board.getBoardCont());
                    board.generate();
                    scene.add(board.getBoardCont());

                })
                $('.edit .east').on('click', function () {
                    $(this).css("background", "rgb(110, 110, 110)")
                    arr[leverData.id].east = "no";
                    scene.remove(board.getBoardCont());
                    board.generate();
                    scene.add(board.getBoardCont());                 
                })
                $('.edit .north').on('click', function () {
                    $(this).css("background", "rgb(110, 110, 110)")
                    arr[leverData.id].north = "no";
                    scene.remove(board.getBoardCont());
                    board.generate();
                    scene.add(board.getBoardCont());                 
                })
                $('.edit .south').on('click', function () {
                    $(this).css("background", "rgb(110, 110, 110)")
                    arr[leverData.id].south = "no";
                    scene.remove(board.getBoardCont());
                    board.generate();
                    scene.add(board.getBoardCont());                 
                })

                $('.edit .close').on('click', function () {
                    $('.edit').css("display", "none");
                })

            }
        }
    })
    $("#LoginBt").on("click", function(){
        time = new Timer()
        $("#root").css("display", "block")
        $("#LoginBox").css("display", "none")
        $("#LeaderBox").css("display", "none")
        userName = $('#LoginIn').val();
        ip = $('#IpIn').val();
    })
    $("#noRefresh").on("click", function(){
        $("#RefreshBox").css("display", "none")
        $("#LeaderBox").css("display", "block")
        $("#LeaderBox").css("top", "25%")
        $("#LeaderBox").css("left", "12.5%")
        $("#LeaderBox").css("width", "75%")
        $("#LeaderBox").css("height", "50%")
        net.getLeaderboard(ip);
    })
    //sterowanie strzałkami
    window.addEventListener('keydown', function (event) {
        looking = event.keyCode;
    })
    window.addEventListener('keyup', function (event) {
        looking = false;
    })


    function render() {
        stats.begin();
        requestAnimationFrame(render)
        renderer.render(scene, camera)
        if (looking == 37) { // left arrow
            camera.rotation.y += Math.PI * player.rotate;
        }
        if (looking == 39) { //right arrow
            camera.rotation.y -= Math.PI * player.rotate;
        }
        if (looking == 38) { //up arrow
            camera.position.x -= Math.sin(camera.rotation.y) * player.speed;
            camera.position.z -= Math.cos(camera.rotation.y) * player.speed;
        }
        if (looking == 40) { //down arrow
            camera.position.x += Math.sin(camera.rotation.y) * player.speed;
            camera.position.z += Math.cos(camera.rotation.y) * player.speed;
        }
        //warunek zwycięstwa
        //9 - ilość klocków na osi x; 4 - ilość klocków na osi z
        if(camera.position.x >= 200 * 9 - 100 && camera.position.z >= 200 * 4 - 100){
            if(endTime == 0){
                endTime = time.getTime()
                time.stopTime()
                $("#RefreshBox").css("display", "block")
                net.time(userName, endTime, ip);
            }
        }

        stats.end();
    }
    render()
}