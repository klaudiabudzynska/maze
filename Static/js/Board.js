function Board() {
    var container = new THREE.Object3D()

    var BoardGeometry = new THREE.PlaneGeometry(5000, 5000, 25, 25);
    var BoardMaterial = new THREE.MeshPhongMaterial({
        color: 0x626975,
        side: THREE.DoubleSide,
        wireframe: false,
        transparent: true,
        opacity: 1,
        shininess: 10
    })
    var Board = new THREE.Mesh(BoardGeometry, BoardMaterial)
    Board.rotateX(Math.PI / 2)
    Board.position.set(0, 0, 0)
    container.add(Board)


    this.generate = function () {
        while (container.children.length > 0) {
            container.remove(container.children[0]);
        }
        container.add(Board)
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].north == "yes") {
                var wall = new Wall()
                wall.getWallCont().position.x = 200 * arr[i].x + 100
                wall.getWallCont().position.z = 200 * arr[i].z
                wall.getWallCont().rotateY(Math.PI / 2)
                container.add(wall.getWallCont())
            }
            if (arr[i].west == "yes") {
                var wall = new Wall()
                wall.getWallCont().position.x = 200 * arr[i].x
                wall.getWallCont().position.z = 200 * arr[i].z - 100
                container.add(wall.getWallCont())
            }
            if (arr[i].south == "yes") {
                var wall = new Wall()
                wall.getWallCont().position.x = 200 * arr[i].x - 100
                wall.getWallCont().position.z = 200 * arr[i].z
                wall.getWallCont().rotateY(Math.PI / 2)
                container.add(wall.getWallCont())
            }
            if (arr[i].east == "yes") {
                var wall = new Wall()
                wall.getWallCont().position.x = 200 * arr[i].x
                wall.getWallCont().position.z = 200 * arr[i].z + 100
                container.add(wall.getWallCont())
            }
            if (arr[i].lever == true) {
                var lever = new Lever();
                lever.getLever().position.x = 200 * arr[i].x
                lever.getLever().position.z = 200 * arr[i].z
                lever.getLever().position.y = 15;
                container.add(lever.getLever())

                lever.getLever().userData = {
                    a: "lever",
                    id: i
                };
            }
        }
    }
    this.generate();

    this.getBoardCont = function () {
        return container
    }
}