arr = [{
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 0,
        "z": 0,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 1,
        "z": 0,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "no",
        "east": "no",
        "x": 2,
        "z": 0,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "no",
        "x": 3,
        "z": 0,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "no",
        "east": "no",
        "x": 4,
        "z": 0,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "yes",
        "east": "no",
        "x": 5,
        "z": 0,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 6,
        "z": 0,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "no",
        "east": "no",
        "x": 7,
        "z": 0,
        "lever": true
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "no",
        "east": "no",
        "x": 8,
        "z": 0,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "yes",
        "east": "no",
        "x": 9,
        "z": 0,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 0,
        "z": 1,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 1,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 2,
        "z": 1,
        "lever": true
    },
    {
        "north": "yes",
        "west": "no",
        "south": "no",
        "east": "yes",
        "x": 3,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 4,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 5,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "yes",
        "east": "no",
        "x": 6,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 7,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 8,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 9,
        "z": 1,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 0,
        "z": 2,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "yes",
        "east": "no",
        "x": 1,
        "z": 2,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 2,
        "z": 2,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 3,
        "z": 2,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "no",
        "east": "no",
        "x": 4,
        "z": 2,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 5,
        "z": 2,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 6,
        "z": 2,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 7,
        "z": 2,
        "lever": true
    },
    {
        "north": "no",
        "west": "no",
        "south": "no",
        "east": "yes",
        "x": 8,
        "z": 2,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "no",
        "east": "no",
        "x": 9,
        "z": 2,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "yes",
        "east": "no",
        "x": 0,
        "z": 3,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "no",
        "east": "yes",
        "x": 1,
        "z": 3,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 2,
        "z": 3,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 3,
        "z": 3,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 4,
        "z": 3,
        "lever": true
    },
    {
        "north": "no",
        "west": "no",
        "south": "no",
        "east": "yes",
        "x": 5,
        "z": 3,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "no",
        "east": "no",
        "x": 6,
        "z": 3,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 7,
        "z": 3,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "no",
        "x": 8,
        "z": 3,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "no",
        "east": "no",
        "x": 9,
        "z": 3,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 0,
        "z": 4,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 1,
        "z": 4,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 2,
        "z": 4,
        "lever": false
    },
    {
        "north": "no",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 3,
        "z": 4,
        "lever": false
    },
    {
        "north": "no",
        "west": "no",
        "south": "no",
        "east": "yes",
        "x": 4,
        "z": 4,
        "lever": false
    },
    {
        "north": "yes",
        "west": "yes",
        "south": "no",
        "east": "yes",
        "x": 5,
        "z": 4,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 6,
        "z": 4,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 7,
        "z": 4,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "yes",
        "x": 8,
        "z": 4,
        "lever": false
    },
    {
        "north": "yes",
        "west": "no",
        "south": "yes",
        "east": "no",
        "x": 9,
        "z": 4,
        "lever": false
    }
];