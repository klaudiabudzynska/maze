function Timer() {
    var start = new Date().getTime()
    var minute
    var second
    var milisecond
    var counting
    var timer = setInterval(function () {
        var now = new Date().getTime()
        counting = now - start
        minute = Math.floor(counting / (1000 * 60))
        second = Math.floor(counting / 1000 - (minute * 60))
        milisecond = Math.floor(counting - (second * 1000) - (minute * 1000 * 60))
        counting = minute + ":" + second + ":" + milisecond
        $("#time").html(counting)
    }, 1)
    this.getTime = function () {
        return counting
    }
    this.stopTime = function(){
        return clearInterval(timer)
    }
}