function Lever() {

    var geometry = new THREE.CylinderGeometry(5, 5, 30, 32);
    var material = new THREE.MeshPhongMaterial({
        color: 0x0000ff,
        side: THREE.DoubleSide,
        shininess: 15
    });
    var cylinder = new THREE.Mesh(geometry, material);

    this.getLever = function () {
        return cylinder;
    }
}