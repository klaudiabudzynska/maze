var http = require("http");
var fs = require("fs");
var qs = require("querystring");
var mongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var Operations = require("./modules/Operations.js");
var _db;
var opers = new Operations();

var users = [];
var server = http.createServer(function (request, response) {

    switch (request.method) {
        case "GET":
            // tu wykonaj załadowanie statycznej strony z formularzem
            if (request.url === "/") {
                fs.readFile("static/index.html", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/css/style.css") {
                fs.readFile("static/css/style.css", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'text/css'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Board.js") {
                fs.readFile("static/js/Board.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Main.js") {
                fs.readFile("static/js/Main.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Wall.js") {
                fs.readFile("static/js/Wall.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Player.js") {
                fs.readFile("static/js/Player.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Net.js") {
                fs.readFile("static/js/Net.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Maze.js") {
                fs.readFile("static/js/Maze.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })

            } else if (request.url === "/js/Lever.js") {
                fs.readFile("static/js/Lever.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/OrbitControls.js") {
                fs.readFile("static/js/OrbitControls.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Stats.js") {
                fs.readFile("static/js/Stats.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })

            } else if (request.url === "/js/Timer.js") {
                fs.readFile("static/js/Timer.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/js/Lights.js") {
                fs.readFile("static/js/Lights.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/libs/jquery.js") {
                fs.readFile("static/libs/jquery.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/libs/three.js") {
                fs.readFile("static/libs/three.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/models/model.js") {
                fs.readFile("static/models/model.js", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'application/javascript'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/models/model.png") {
                fs.readFile("static/models/model.png", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'image/png'
                    });
                    response.write(data);
                    response.end();
                })
            } else if (request.url === "/models/wall.jpg") {
                fs.readFile("static/models/wall.jpg", function (error, data) {
                    response.writeHead(200, {
                        'Content-Type': 'image/jpg'
                    });
                    response.write(data);
                    response.end();
                })
            }
            break;
        case "POST":
            var allData = "";
            request.on("data", function (data) {
                allData += data;
            })
            request.on("end", function (data) {
                var finishObj = qs.parse(allData)
                switch (finishObj.akcja) {
                    //dodanie nowego usera
                    case "DODAJ_UZYTKOWNIKA":

                        if (users.length < 2) {
                            users.push(1);
                            response.end(JSON.stringify({
                                a: finishObj.user,
                                b: users.length
                            }));
                        } else {
                            response.end(JSON.stringify({
                                a: users.length
                            }));
                        }
                        break;
                        //inna akcja
                    case "CZEKAMY":
                        response.end(JSON.stringify({
                            a: users.length
                        }));
                        break;
                    case "CZAS":
                        mongoClient.connect("mongodb://localhost/labiryncik", function (err, db) {
                            if (err) console.log(err)
                            else console.log("mongo podłączone")
                            //tu można operować na utworzonej bazie danych db lub podstawić jej obiekt 
                            // pod zmienną widoczną na zewnątrz    
                            _db = db;
                            db.createCollection("czasy", function (err, coll) {
                                coll.insert({
                                    user: finishObj.user,
                                    time: finishObj.time
                                }, function (err, result) {
                                    console.log(result)
                                });
                            })
                        })
                        break;
                    case "LEADERS":
                        mongoClient.connect("mongodb://localhost/labiryncik", function (err, db) {
                            if (err) console.log(err)
                            else console.log("mongo podłączone")
                            //tu można operować na utworzonej bazie danych db lub podstawić jej obiekt 
                            // pod zmienną widoczną na zewnątrz    
                            _db = db;
                            db.createCollection("czasy", function (err, coll) {
                                opers.SelectAll(coll, function (data) {
                                    console.log(data)
                                    response.end(JSON.stringify(data));
                                })
                            })
                        })
                        break;
                }
            })

            break;

    }



    // parametr res oznacza obiekt odpowiedzi serwera (response)
    // parametr req oznacza obiekt żądania klienta (request)

})

server.listen(3000, function () {
    console.log("serwer startuje na porcie 3000")
});